# white.panda
Created Rest API's to show, add, update, delete cars and their bookings. 
Tech Stack used: Node.js, Express.js, MongoDB
Hosted it on heroku and mlab.<br />

https://damp-fortress-07566.herokuapp.com/
<br />

API Endpoints and their definition-
<br />

https://damp-fortress-07566.herokuapp.com/cars<br />
This would get the list of cars from the Database.
We can also post additional cars from Postman, Insomnia or the terminal using the POST method.
<br />
Delete Http operation would delete cars.
<br />
Put Http operation would update the modelof the car.
<br />
https://damp-fortress-07566.herokuapp.com/booking
<br />
This would get the list of boookings from the Database.
We can also post additional cars from Postman, Insomnia or the terminal using the POST method.

To Book a specific car based on its availability.<br />
https://damp-fortress-07566.herokuapp.com/cars/AA<br />
where AA is the car name, this would extend the availability of the last user by 5 days.

This would show the cars above a specific seating limit.
<br />
https://damp-fortress-07566.herokuapp.com/cars/4
<br />
In this case cars above 4 seats would be shown.

I've used the Mongoose abstraction over the MongoDB database which is hosted on Mlab.<br />
I've defined some methods in the controller class, this would help facilitate testing and gives some level of abstraction. 


Things to do:<br />
Add some tests usign Mocha and Sinon in JS.<br />
Add JWT's for authorization. <br />
Add some basic UI.



