//creating the schema to put in the cars database

const mongoose = require('mongoose');

const { Schema } = mongoose;

const bookingModel = new Schema(
    {
        Name: {type: String}, 
        Model: {type: String},
        PhoneNumber: {type: Number}, 
        IssueDate: {type: Date},
        ReturnDate: {type: Date}

    }
);

module.exports = mongoose.model('Booking', bookingModel);