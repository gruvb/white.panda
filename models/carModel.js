//creating the schema to put in the cars database

const mongoose = require('mongoose');

const { Schema } = mongoose;

const carModel = new Schema(
    {
        model: {type: String}, 
        number: {type: Number}, 
        seating: {type: Number},
        RentperDay: {type: Number}

    }
);

module.exports = mongoose.model('Cars', carModel);
