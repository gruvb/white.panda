//adding functions for get and post, so that it becomes easier for testing

function carsController(Cars) {
    function post(req, res) {
        const cars = new Cars(req.body);
        //mapping of ORM with the schema
        cars.save();
        //saving it to database
        console.log(cars);
        res.status(201);
        return res.json(cars);
    }
    function get(req, res) {
        const query = req.query;

        Cars.find(query, (err, cars) => {
            if (err) {
                return res.send(err);
            }
            return res.json(cars);
        });
    }
    return { post, get };

    }
module.exports = carsController;