//adding functions for get and post, so that it becomes easier for testing

function bookingController(Booking) {
    function post(req, res) {
        const bookings = new Booking(req.body);
        //mapping of ORM with the schema
        bookings.save();
        //saving it to databasecs
        console.log(bookings);
        res.status(201); 
        return res.json(bookings);
    }
    function get(req, res) {
        const query = req.query;
        Booking.find(query, (err, bookings) => {
            if (err) {
                return res.send(err);
            }
            return res.json(bookings);
        });
    }
    return { post, get };

    }
module.exports = bookingController;