const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
// const db = mongoose.connect('mongodb://localhost/cars');
const db = mongoose.connect('mongodb+srv://##.##@cluster0-wm0el.mongodb.net/test?retryWrites=true&w=majority');
const Cars = require('./models/carModel');
const Booking = require('./models/bookingModel');
const carsController = require('./controller/carsController.js')(Cars)
const bookingController = require('./controller/bookingController.js')(Booking)

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.send('Hello')
});

app.get('/cars', function(req, res){
    carsController.get(req, res);
});
app.post('/cars', function(req, res) {
    carsController.post(req, res);
});

app.get('/booking', function(req, res){
    bookingController.get(req, res);
})
app.post('/booking', function(req, res) {
    bookingController.post(req, res);
});
var jsonData = [];
//Book a specific car based on availability 
app.get('/booking/:model', function(req, res) {
    // bookingController.available(req, res);
    Booking.find({Model:req.params.model}, (err, bookings) => {
        if (err) {
            return res.send(err);
        }
        // console.log(bookings[0].IssueDate);
        for(var i = 0; i<bookings.length; i++){
            var obj = bookings[i];
            jsonData.push(bookings[i].ReturnDate);
        }
        var tomorrow = bookings[1].ReturnDate;
        tomorrow.setDate(tomorrow.getDate() + 15);
        console.log(tomorrow);
        var name = bookings[0].name;
        var model = req.params.model;
        var phoneNumber = bookings[0].PhoneNumber;
        var issueDate = tomorrow;
        var returnDate = tomorrow.setDate(tomorrow.getDate() + 20);

        var Data = {Name : name, Model : model, PhoneNumber : phoneNumber, IssueDate : issueDate, ReturnDate : returnDate};
        Booking.insertMany([Data]);
        //store the result in an array
        // for(var i in jsonData){
        //     console.log(jsonData[i]);
        // }


        return res.json(bookings);
    });
    //check for available dates for that model
    //book it for one more day by adding it's availability
})
//showing cars with available seats
app.get('/cars/:seating', function(req, res){
    const query = req.query;
    var carsData = [];
    const seats = req.params.seating;
    Cars.find(query, (err, cars) => {
        if (err) {
            return res.send(err);
        }
        for(var i = 0; i<cars.length; i++){
            var obj = cars[i];
            if(cars[i].seating >= seats) {
                carsData.push(cars[i]);
            }
        }
        // console.log(cars[0].seating);
        return res.json(carsData);
    });
});


//delete a car when not available
app.delete('/cars/:Model', function(req, res) {
    const query = req.query;
    Booking.find(query, function(err, booking) {
        if (err) {
            return res.send(err);
        }
        for(var i in booking){
            console.log(booking[i].Model);
            if(booking[i].Model == req.params.Model) {
                console.log('Found one');
                console.log(i);
                return res.sendStatus(404);
                // console.log(req.params.Model);
            }
            else {
                Cars.remove({model : req.params.Model}, function(err, cars) {
                    return res.sendStatus(204);
                })
                
            }
        }
        // console.log(booking);
    })
})
app.put('/cars/:model/:NewModel', function(req, res) {
    const query = req.query;
    Cars.find(query, function(err, cars) {
        if(err) {
            return res.send(err);
        }
        for(var i = 0; i<cars.length; i++){
            console.log(cars[i])
            if(cars[i].model == req.params.model) {
                cars[i].model = req.params.NewModel;
            }
        }
        return res.json(cars);
    })
})
//updating a car




console.log(jsonData.length);

//delete a car

//delete a booking

app.listen(port, function() {
    console.log('Listening on' + port);}
); 